#!/usr/bin/python

import sqlite3
import sys

conn = None

try:
    conn = sqlite3.connect('test.db')

    cur = conn.cursor()

    cur.execute('SELECT SQLITE_VERSION()')
    ver = cur.fetchone()
    print("SQLite version: %s" % ver)

    # Create a new table 'Writers'
    cur.execute("CREATE TABLE Writers(Id INTEGER PRIMARY KEY AUTOINCREMENT, \
             Name VARCHAR(25))")
    cur.execute("INSERT INTO Writers(Name) VALUES('Jack London')")
    cur.execute("INSERT INTO Writers(Name) VALUES('Honore de Balzac')")
    cur.execute("INSERT INTO Writers(Name) VALUES('Lion Feuchtwanger')")

    # Retrieve data
    for row in cur.execute("SELECT * FROM Writers"):
        print(row)

    cur.execute("DROP TABLE Writers")

except (sqlite3.Error) as e:
    print("Error %s:" % e.args[0])
    sys.exit(1)

finally:

    if conn:
        conn.close()
